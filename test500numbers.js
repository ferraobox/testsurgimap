module.exports = {
  fillArray: () =>
    Array(500)
      .fill(0)
      .map(() => Math.random() * 100),

  getSmallest: arr => arr.reduce((previous, current) => (current < previous ? current : previous), 99)
};
