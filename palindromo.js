module.exports = word => {
  return word.toLowerCase().replace(' ', '') ===
    word
      .toLowerCase()
      .replace(' ', '')
      .split('')
      .reverse()
      .join('')
    ? true
    : false;
};
