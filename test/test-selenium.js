const makeTest = require('../helpers/setup').makeTest;
const assert = require('chai').assert;
const expect = require('chai').expect;
const data = require('../pageObjects/data');
const homepage = require('../pageObjects/homepage');
const loginpage = require('../pageObjects/loginpage');
const registerpage = require('../pageObjects/registerpage');
const startserver = require('../services/serverMail');

makeTest('- Test Surgimap -', function(setupdriver) {
  const driver = setupdriver;
  const home = new homepage(driver);
  const login = new loginpage(driver);
  const register = new registerpage(driver);

  it('1. Navigate to random https://www.surgimap.com/', async function() {
    await home.getPage();
    return driver;
  });

  it('2. Verify loaded page title is “Surgimap.com: Official Site for Surgimap”', async function() {
    expect(await home.getTitle()).to.equal('Surgimap.com: Official Site for Surgimap');
    return driver;
  });

  it('3. On the right top corner of the page, click on “Login”', async function() {
    await home.clickLogin();
    return driver;
  });

  it('4. Verify you landed on Login page', async function() {
    expect(await login.buttonCreateAccountDisplayed()).to.be.true;
    return driver;
  });

  it('5. On Login page click “Create an account”', async function() {
    await login.clickCreateAccount();
    return driver;
  });

  it('6. Verify you landed on Create an Account page', async function() {
    expect(await register.buttonRegisterDisplayed()).to.be.true;
    return driver;
  });

  it('7. Fill in all fields and click “Create an Account”', async function() {
    await register.fillemail(data.user);
    await register.fillpass(data.user);
    await register.fillcomfirmpass(data.user);
    await register.clickCheckTerms();
    await register.clickCheckPrivacy();
    await register.clickCheckServices();
    await register.clickCheckBussines();
    await driver.sleep(3000);
    await register.clickButtonRegister();
    await register.buttonComfirmRegisterDisplayed();
    await register.clickComfirmRegister();
    return driver;
  });

  it('8. Verify that Activation email is appeared in mailbox you entered in Step 7', async function() {
    const mail = await startserver();
    console.log(mail);
    expect(mail).to.equal('Your Surgimap Registration');
  });
});
