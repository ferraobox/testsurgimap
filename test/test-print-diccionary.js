const expect = require('chai').expect;
const printdiccionary = require('../printfile');
const doesFileExist = require('../doesFileExist');

describe('- Test Files -', function() {
  it('Test incorrect path "wrong.txt"', async function() {
    expect(await doesFileExist('./wrong.txt')).to.throw;
  });

  it('Test correct path "file.txt"', async function() {
    expect(await doesFileExist('./file.txt')).to.be.true;
  });

  it('Print diccionary from "file.txt"', async function() {
    await printdiccionary('./file.txt');
  });
});
