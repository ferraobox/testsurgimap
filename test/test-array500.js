const expect = require('chai').expect;
const { fillArray, getSmallest } = require('../test500numbers');

describe('- Test smallest of 500 -', function() {
  it('Fill array with 500 random integers from 0 to 100 ', function() {
    let arr = fillArray();
    expect(arr.length).to.equal(500);
  });
  it('Print the smallest of 500', function() {
    let arr = fillArray();
    const smallest = getSmallest(arr);
    console.log('Smallest:', smallest);
  });
});
