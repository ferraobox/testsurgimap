const expect = require('chai').expect;
const palindromo = require('../palindromo');

describe('- Test Palindromo -', function() {
  it('Test - Word Aerea', function() {
    expect(palindromo('Aerea')).to.be.true;
  });
  it('Test - Word Race Car', function() {
    expect(palindromo('Race Car')).to.be.true;
  });
  it('Test - Word Fail', function() {
    expect(palindromo('Fail')).to.be.false;
  });
});
