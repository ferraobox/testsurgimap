const readline = require('readline');
const fs = require('fs');

module.exports = path => {
  const rl = readline.createInterface({
    input: fs.createReadStream(path),
    crlfDelay: Infinity
  });
  rl.on('line', line => {
    const [word, definitions] = line.split('-');
    console.log(word);
    definitions.split(',').forEach(ele => console.log(ele));
  });
};
