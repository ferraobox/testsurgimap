const { exists } = require('fs');
const { promisify } = require('util');
const existsPromise = promisify(exists);

module.exports = path =>
  existsPromise(path).catch(err => {
    throw err;
  });

// const fs = require('fs');
// module.exports = path => {
//   return fs.existsSync(path, (err, result) => {
//     if (err) throw err;
//     console.log(result);
//     return result;
//   });
// };
