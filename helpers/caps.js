const chromeArgsWithoutUI = ['--disable-infobars', '--headless', 'disable-gpu', 'disable-extensions', 'verbose', 'no-sandbox'];
const chromeArgs = ['--disable-infobars', 'disable-extensions', 'verbose', 'no-sandbox'];
exports.chrome = {
  browserName: 'chrome',
  shardTestFiles: true,
  javascriptEnabled: true,
  acceptSslCerts: true,
  chromeOptions: {
    //, '--start-fullscreen'
    args: chromeArgs,
    prefs: {
      // disable chrome's annoying password manager
      'profile.password_manager_enabled': false,
      credentials_enable_service: false,
      password_manager_enabled: false
    }
  }
};
