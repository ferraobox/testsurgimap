require('colors');
const caps = require('../helpers/caps').chrome;
const { Builder } = require('selenium-webdriver');

//Make test hooks are executed before and after of each test, for hooks before the execution, you should go to mocha.prepare
const makeTest = function(desc, cb) {
  describe(desc, function() {
    this.timeout(800000);
    let driver = new Builder()
      .forBrowser('chrome')
      .setChromeOptions(caps)
      .build();

    before(function() {});

    beforeEach(function() {});

    after(function() {
      return driver.quit();
    });

    afterEach(function() {});

    cb(driver);
  });
};

exports.makeTest = makeTest;
