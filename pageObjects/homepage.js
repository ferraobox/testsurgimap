const { By, Key, until } = require('selenium-webdriver');
module.exports = class Homepage {
  constructor(driver) {
    this.d = driver;
    this.loginbutton = 'a[class="btn-login"]';
  }

  getPage() {
    return this.d.get('https://www.surgimap.com/');
  }

  getTitle() {
    return this.d.getTitle();
  }

  clickLogin() {
    return this.d.findElement(By.css(this.loginbutton)).click();
  }

  async buttonLoginDisplayed() {
    const loginbutton = await this.d.findElement(By.css(this.loginbutton));
    return this.d.wait(until.elementIsVisible(loginbutton), 20000);
  }
};
