const { By, Key, until } = require('selenium-webdriver');
module.exports = class Loginpage {
  constructor(driver) {
    this.d = driver;
    this.subheader = 'p[class="sub-header"]';
    this.createaccount = '#aCreateAccount';
  }

  getSubHeader() {
    return this.d.findElement(By.css(this.subheader)).getText();
  }

  clickCreateAccount() {
    return this.d.findElement(By.css(this.createaccount)).click();
  }

  async buttonCreateAccountDisplayed() {
    return this.d.wait(until.elementLocated(By.css(this.createaccount)), 20000).then(() => {
      return this.d.findElement(By.css(this.createaccount)).isDisplayed();
    });
  }
};
