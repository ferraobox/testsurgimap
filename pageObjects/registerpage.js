const { By, Key, until } = require('selenium-webdriver');
module.exports = class Registerpage {
  constructor(driver) {
    this.d = driver;
    this.registerheader = 'p[class="register-header"]';
    this.emailfield = '#txtEmail';
    this.passfield = '#txtPassword';
    this.comfirmpassfield = '#txtConfirmPassword';
    this.buttonregister = '#btnRegister > span';
    this.checkterms = '#terms1';
    this.checkprivacy = '#terms4';
    this.checkservices = '#terms2';
    this.checkbussines = '#terms3';
    this.comfirmregister = 'button.swal2-confirm.swal2-styled';
  }

  getSubHeader() {
    return this.d.findElement(By.css(this.registerheader)).getText();
  }

  async buttonRegisterDisplayed() {
    return this.d.wait(until.elementLocated(By.css(this.buttonregister)), 20000).then(() => {
      return this.d.findElement(By.css(this.buttonregister)).isDisplayed();
    });
  }

  clickButtonRegister() {
    return this.d.findElement(By.css(this.buttonregister)).click();
  }

  fillemail(user) {
    return this.d.findElement(By.css(this.emailfield)).sendKeys(user.mail);
  }

  fillpass(user) {
    return this.d.findElement(By.css(this.passfield)).sendKeys(user.pass);
  }

  fillcomfirmpass(user) {
    return this.d.findElement(By.css(this.comfirmpassfield)).sendKeys(user.pass);
  }

  clickCheckTerms() {
    return this.d.findElement(By.css(this.checkterms)).click();
  }

  clickCheckPrivacy() {
    return this.d.findElement(By.css(this.checkprivacy)).click();
  }

  clickCheckServices() {
    return this.d.findElement(By.css(this.checkservices)).click();
  }

  clickCheckBussines() {
    return this.d.findElement(By.css(this.checkbussines)).click();
  }

  clickComfirmRegister() {
    return this.d.findElement(By.css(this.comfirmregister)).click();
  }

  async buttonComfirmRegisterDisplayed() {
    return this.d.wait(until.elementLocated(By.css(this.comfirmregister)), 20000).then(() => {
      return this.d.findElement(By.css(this.comfirmregister)).isDisplayed();
    });
  }
};
